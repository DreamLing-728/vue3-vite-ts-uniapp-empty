export const downloadUrl = (urlPath) => {
  // 下载图片
  const ua = window.navigator.userAgent.toLowerCase()
  // 微信内置浏览器
  if (ua.indexOf('micromessenger') != -1) {
    uni.showToast({
      title: '请点击右上角在浏览器打开进行文件下载',
      icon: 'none',
    })
  } else {
    const url = urlPath
    const dload = document.createElement('a')
    dload.download = '' // 设置下载的文件名，默认是'下载'
    dload.href = url
    document.body.appendChild(dload)
    dload.click()
    dload.remove() // 下载之后把创建的元素删除
  }
}
