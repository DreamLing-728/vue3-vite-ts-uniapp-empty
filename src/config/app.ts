export const APP_NAME = '截屏'

export const IMAGE_URL = 'https://mini-1304817606.file.myqcloud.com' // 静态资源的cos地址

export const BASE_URL = '/dev-api'

export const HEADER = {
  'content-type': 'application/json',
}

export const HEADERPARAMS = {
  'content-type': 'application/x-www-form-urlencoded',
}

export const TOKENNAME = 'Authorization'
