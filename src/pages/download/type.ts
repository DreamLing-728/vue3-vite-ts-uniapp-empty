export type FILE_MSG = {
  fileName?: string
  filePath?: string
}
