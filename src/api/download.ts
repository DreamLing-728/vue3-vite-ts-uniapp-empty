import request from '../utils/request'

/**
 * 获取截屏图片 -- 陈锐煌
 *
 */
export function getScreenshot(urlParams) {
  return request?.get?.(`/screenshot/${urlParams}`, {}, { noAuth: true })
}
